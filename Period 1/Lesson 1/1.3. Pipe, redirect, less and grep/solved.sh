# test_folderində olan faylları for ilə sadalayın və hər birinin daxilinə faylın öz adını yazın

cd ~/test_folder

for f in $(ls ./)
    do
        echo ${f} > ${f}
    done

# test_folderində olan bütün faylların içərisindəki mətnləri print edin
cat *

# test_folderində olan faylları for ilə sadalayın və hər birinin sonuna faylın öz adını əlavə edin

for f in $(ls ./)
    do
        echo ${f} >> ${f}
    done

# test_folderində olan sonu 3 ilə bitən faylların içərisindəki mətnləri print edin
cat tmp_file_*3.txt

# test_folderində olan bütün faylların içərisindəki mətnləri cat və less komandasının köməyi ilə hissə-hissə print edin
cat * | less

# cat və grep əmrinin köməkliyi ilə içərisində 4.txt mətni olan bütün faylları print edin
cat * | grep "4.txt"
