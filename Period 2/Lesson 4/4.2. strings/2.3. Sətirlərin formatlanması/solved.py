# -*- coding: utf-8 -*-

from string import Template

# 1. "%s %x" % (var1, var2) üsulu ilə username və orderNo dəyişənlərini salamlama mətninə əlavə edərək istifadəçini salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
greetingMessage = "Hörmətli %s, xoş gəlmisiniz. Sizin sifarişiniz: 0x%x" % (username, int(orderNo))
print("1.", greetingMessage)

# 2. "%(var1)s %(var2)x" % (var1, var2) üsulu ilə username və orderNo dəyişənlərini salamlama mətninə əlavə edərək istifadəçini salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
greetingMessage = "Hörmətli %(name)s, xoş gəlmisiniz. Sizin sifarişiniz: 0x%(order)x" % {"name":username, "order": int(orderNo)}
print("2.", greetingMessage)

# 3. "{} {:x}".format(var1, var2) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
greetingMessage = "Hörmətli {}, xoş gəlmisiniz. Sizin sifarişiniz: 0x{:x}".format(username, int(orderNo))
print("3.", greetingMessage)

# 4. "{var1} {var2:x}".format(var1=var1, var2=var2) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
greetingMessage = "Hörmətli {name}, xoş gəlmisiniz. Sizin sifarişiniz: 0x{order:x}".format(name=username, order=int(orderNo))
print("4.", greetingMessage)

# 5. f"{var1} {var2:#x}" metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
age = int(input("Doğulduğunuz ili daxil edin: "))
orderNo = int(input("Sifariş nömrənizi daxil edin: "))
greetingMessage = f"Hörmətli {username}, xoş gəlmisiniz. Sizin yaşınız: {2018-age}. Sifariş nömrəsi: {orderNo:#x}"
print("5.", greetingMessage)

# 6. Template("$var").substitude(var=var) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
age = int(input("Doğulduğunuz ili daxil edin: "))
orderNo = int(input("Sifariş nömrənizi daxil edin: "))
greetingMessage = Template("Hörmətli $username, xoş gəlmisiniz. Sizin yaşınız: $age. Sifariş nömrəsi: $orderNo")
print("6.", greetingMessage.substitute(username=username, age=2018-age, orderNo=hex(orderNo)))