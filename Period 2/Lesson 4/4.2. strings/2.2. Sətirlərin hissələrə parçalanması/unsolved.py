# -*- coding: utf-8 -*-

myString = "SALAM DÜNYA"

# 1. myString[<start>:<end>:step] üsulundan istifadə edərək Ü hərfini çap edin
# kodu buraya yazın

# 2. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM cümləsini a dəyişəninə, DÜNYA sözünü b dəyişəninə mənimsədərək hər birini ekranda çap edin
# kodu buraya yazın

# 3. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsini tərsinə ekranda çap edin
# kodu buraya yazın

# 4. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsindən birinci simvol daxil olmaqla hər 3-cü simvolu çap edin
# kodu buraya yazın

# 5. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsinin hər 2-ci simvolunu, elə 2-ci simvoldan başlamaq şərti ilə çap edin
# kodu buraya yazın

# 6. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsinin axırıncı simvol istisna olmaqla son 3 simvolunu ekranda çap edin
# kodu buraya yazın