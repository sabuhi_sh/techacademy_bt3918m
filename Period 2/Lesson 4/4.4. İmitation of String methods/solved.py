# -*- coding: utf-8 -*-

### Funksiyalar haqda daha ətraflı python dilinin rəsmi dokumentasiya səhifəsindən öyrənə bilərsiniz:
### Link: https://docs.python.org/2/library/string.html#string-functions

# center(width, fillchar) metodunu öz kodlarınızla yazın
# ===========================================
myStr = "TechAcademy"
width = 20
fillchar = "="
width = width - len(myStr)
left = width // 2
right = width - left

for i in range(left):
    myStr = fillchar + myStr

for i in range(right):
    myStr += fillchar

print(myStr)
# ===========================================

# join(seq) metodunu öz kodlarınızla yazın
# ===========================================
myStr = "TechAcademy"
str = "="
newStr = ""
for i in myStr[:-1]:
    newStr += i + str
newStr += myStr[-1]

print(newStr)
# ===========================================

# zfill metodunu öz kodlarınızla yazın
# ===========================================
myStr = "TechAcademy"
length = 20
remain = length - len(myStr)
for i in range(remain):
    myStr = "0" + myStr
print(myStr)
# ===========================================










